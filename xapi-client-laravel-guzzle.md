# Client xAPI avec Laravel et Guzzle

## Installation de Guzzle
`composer require guzzlehttp/guzzle`

## Exemple de controleur Laravel
```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class TestController extends Controller
{
    /**
     * LRS endpoint.
     */
    public $endpoint;

    /**
     * HTTP service.
     */
    protected $http;

    /**
     * Basic HTTP authorization.
     */
    protected $auth;
    

    /**
     * Construct.
     */
    public function __construct()
    {
        $this->endpoint = config('trax.lrs.endpoint');
        $this->http = new Client();
        $this->auth = 'Basic '.base64_encode(config('trax.lrs.key') . ":" . config('trax.lrs.secret'));
    }
    
    /**
     * Get the 10 last statements.
     */
    public function getStatements(Request $req)
    {
        $response = $this->http->get($this->endpoint.'/statements', [
            'headers' => [
                'X-Experience-API-Version' => '1.0.3',
                'Authorization' => $this->auth,
            ],
            'query' => ['limit' => 10],
        ]);
        
        dd(json_decode($response->getBody()));
    }

    /**
     * Post statements.
     */
    public function postStatements(Request $req)
    {
        $statement = [
            'actor' => ['mbox' => 'mailto:learner1@xapi.fr'],
            'verb' => ['id' => 'http://adlnet.gov/expapi/verbs/completed'],
            'object' => ['id' => 'http://xapi.fr/activities/act01'],
        ];
        
        $response = $this->http->post($this->endpoint.'/statements', [
            'headers' => [
                'X-Experience-API-Version' => '1.0.3',
                'Authorization' => $this->auth,
            ],
            'json' => $statement,
        ]);
        
        dd(json_decode($response->getBody()));
    }
}

```

